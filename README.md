# O que é aprendizado de máquina?
_Baseado no texto escrito por Jason Brownlee em 17 de novembro de 2013_.

Se você estiver interessado em Machine Learning e se dedicar um pouco ao tema possivelmente você irá falar sobre Machine Learning com um amigo ou colega um dia. Você correrá o risco de alguém realmente perguntar:

“ Então, o que é aprendizado de máquina? “

O objetivo deste breve texto é apresentar algumas definições para pensar e dar uma breve e acessível definição de uma linha que seja fácil de se lembrar.

Começaremos com as definições padrão de Aprendizado de Máquina retiradas de conhecidos livros-texto. Terminaremos desenvolvendo uma definição de desenvolvedores de aprendizado de máquina e uma prática de uma linha que podemos usar sempre que nos perguntarem: O que é Aprendizado de Máquina?

## Definições autorarias
Vamos começar examinando quatro livros didáticos sobre Machine Learning que são comumente usados ​​em cursos de nível universitário.

Estas são nossas definições autorarias e estabelecem nossa base para um pensamento mais profundo sobre o assunto.

Estas quatro definições destacam algumas perspectivas úteis e variadas sobre o tema. Através da experiência, aprenderemos que o tema é realmente uma confusão de métodos e escolher uma perspectiva é importante para progredir com seu entendimento.

### Aprendizado de Máquina de Mitchell

_Tom Mitchell_ em seu livro **Machine Learning** fornece uma definição no prefácio:

**O campo do aprendizado de máquina está preocupado com a questão de como construir programas de computador que melhoram automaticamente com a experiência.**

Esta definição curta e doce e é a base para a definição de desenvolvedores que destacada no final do texto.

Observe a menção de “ programas de computador ” e a referência a “ melhoria automatizada ”. Escreva programas que melhorem a si mesmos, é provocativo!

Em sua introdução, ele fornece um pequeno formalismo que você verá muitas repetidas vezes:

Um programa de computador é dito para aprender com a experiência E com relação a alguma classe de tarefas T e medida de desempenho P, se o seu desempenho em tarefas em T, medida pelo P, melhora com a experiência E.
Não deixe a definição de termos te assustar, este é um formalismo muito útil.

Podemos usar esse formalismo como modelo e colocar E, T e P no topo das colunas de uma tabela e listar problemas complexos com menos ambigüidade. Ele poderia ser usado como uma ferramenta de design para nos ajudar a pensar claramente sobre quais dados coletar ( E ), quais decisões o software precisa tomar ( T ) e como avaliaremos seus resultados ( P ). Este poder é porque é frequentemente repetido como uma definição padrão. Guarde-o no bolso de trás.

### Elementos de Aprendizagem Estatística

**Os elementos do aprendizado estatístico: mineração de dados, inferência e previsão** foi escrito por três estatísticos de Stanford e auto-descritos como uma estrutura estatística para organizar seu campo de investigação.

No prefácio está escrito:

**Vastas quantidades de dados estão sendo geradas em muitos campos, e o trabalho dos estatísticos é dar sentido a tudo: extrair padrões e tendências importantes e entender “o que os dados dizem”. Nós chamamos esse aprendizado de dados.**

Entende-se que o trabalho de um estatístico é usar as ferramentas de estatísticas para interpretar dados no contexto do domínio. Os autores parecem incluir todo o campo de Machine Learning como auxiliares nessa busca. Curiosamente, eles optaram por incluir " Data Mining " no subtítulo do livro.

Os estatísticos aprendem com os dados, mas o software também, e aprendemos com as coisas que o software aprende. Das decisões tomadas e dos resultados alcançados pelos vários métodos de aprendizado de máquina.

#### Reconhecimento de padrões
_Bishop_ no prefácio de seu livro  **_Pattern Recognition e Machine Learning_** comentários:

**O reconhecimento de padrões tem suas origens na engenharia, enquanto o aprendizado de máquina cresceu a partir da ciência da computação. No entanto, essas atividades podem ser vistas como duas facetas do mesmo campo ...**

Lendo isto, você tem a impressão de que Bishop veio a campo de uma perspectiva de engenharia e depois aprendeu e aproveitou a Ciência da Computação, assumindo os mesmos métodos. O reconhecimento de padrões é um termo de engenharia ou processamento de sinais.

Esta é uma abordagem madura e devemos ser imitadas. De maneira mais ampla, independentemente do campo que reivindica um método, se ele atende às nossas necessidades aproximando-nos de um insight ou de um resultado "aprendendo com os dados", podemos decidir chamar isso de aprendizado de máquina.

### Uma perspectiva algorítmica
_Marsland_ adota a definição de _Mitchell_ em seu livro **Machine Learning: An Algorithmic Perspective**.

Ele fornece uma nota convincente em seu prólogo que motiva sua escrita do livro:

**Uma das características mais interessantes do aprendizado de máquina é que ele está no limite de várias disciplinas acadêmicas, principalmente ciência da computação, estatística, matemática e engenharia. … O aprendizado de máquina é usualmente estudado como parte da inteligência artificial, o que o coloca firmemente na ciência da computação… entender por que esses algoritmos funcionam requer uma certa quantidade de sofisticação estatística e matemática que muitas vezes falta aos graduandos da ciência da computação.**

Isso é perspicaz e instrutivo.

Em primeiro lugar, ele ressalta a natureza multidisciplinar do campo. Nós estávamos tendo um sentimento para isso a partir da definição acima, mas ele desenha um grande sublinhado vermelho para nós. A Aprendizagem de Máquina baseia-se em todos os tipos de ciências da informação.

Em segundo lugar, ele ressalta o perigo de se agarrar a uma determinada perspectiva com muita força. Especificamente, o caso de um algoritmo que foge do funcionamento matemático interno de um método.

Sem dúvida, o caso contrário do estatístico que foge das preocupações práticas de implementação e implantação é igualmente limitante.

Diagrama de Venn
Drew Conway criou um bom Diagrama de Venn  em setembro de 2010 que pode ajudar.

Em sua explicação, ele comenta: Machine Learning = Hacking + Math & Statistics

Diagrama de Venn da Data Science
Diagrama de Venn da Data Science. Creditado a Drew Conway, Creative Commons, licenciado como Attribution-NonCommercial.

Ele também descreve a  Zona de Perigo  como  Skill Skills  +  Expertise .

Aqui, ele está se referindo àquelas pessoas que sabem o suficiente para serem perigosas. Eles podem acessar e estruturar dados, eles conhecem o domínio e podem executar um método e apresentar resultados, mas não entendem o que os resultados significam. Eu acho que é isso que Marsland pode estar insinuando.

### Definição de desenvolvedores de aprendizado de máquina
Agora nos voltamos para a necessidade de dividir tudo isso em porcas e parafusos para os desenvolvedores.

Primeiro, analisamos problemas complexos que resistem às nossas soluções de decomposição e procedimentos. Isso enquadra o poder do aprendizado de máquina. Em seguida, elaboramos uma definição que se encaixa bem com os desenvolvedores que podemos usar sempre que nos perguntam: “ Então, o que é Aprendizado de Máquina? ”Por outros desenvolvedores.

Problemas complexos

Como desenvolvedor, você acabará encontrando classes de problemas que teimosamente resistem a uma solução lógica e procedural.

O que quero dizer é que há classes de problemas em que não é viável ou econômico sentar e escrever todas as instruções if necessárias para resolver o problema.

“ Sacrilégio! Eu ouço o cérebro do seu desenvolvedor gritar.

É verdade.

Considere o caso diário do problema de decisão de discriminar e-mails de spam de e-mails que não são spam. Este é um exemplo usado o tempo todo ao introduzir o aprendizado de máquina. Como você escreveria um programa para filtrar e-mails à medida que chegam à sua conta de e-mail e decide colocá-los na pasta de spam ou na pasta de entrada?

Você provavelmente começaria coletando alguns exemplos e dando uma olhada neles e pensaria profundamente neles. Você procuraria padrões nos e-mails que são spam e aqueles que não são. Você pensaria em abstrair esses padrões para que sua heurística funcionasse com novos casos no futuro. Você ignoraria e-mails estranhos que nunca serão vistos novamente. Você iria para vitórias fáceis para obter sua precisão e criar coisas especiais para os casos de ponta. Você revisaria o e-mail frequentemente ao longo do tempo e pensaria em abstrair novos padrões para melhorar a tomada de decisões.

Há um algoritmo de aprendizado de máquina lá, entre tudo isso, exceto que foi executado pelo programador em vez do computador. Esse sistema codificado manualmente derivado seria tão bom quanto a capacidade do programador de extrair regras dos dados e implementá-los no programa.

Isso poderia ser feito, mas precisaria de muitos recursos e seria um pesadelo de manutenção.

## Aprendizado de Máquina

No exemplo acima, tenho certeza de que o cérebro de seu desenvolvedor, a parte do seu cérebro que impiedosamente busca automatizar, pode ver a oportunidade de automatizar e otimizar o metaprocesso de extrair padrões de exemplos.

Métodos de aprendizado de máquina é esse processo automatizado.

Em nosso exemplo de spam / não spam, os exemplos ( E ) são emails que coletamos. A tarefa ( T ) era um problema de decisão (chamado classificação) de marcar cada e-mail como spam ou não e colocá-lo na pasta correta. Nossa medida de desempenho ( P ) seria algo como precisão como uma porcentagem (decisões corretas divididas pelo total de decisões tomadas multiplicadas por 100) entre 0% (pior) e 100% (melhor).

A preparação de um programa de tomada de decisão como esse geralmente é chamado de treinamento, em que os exemplos coletados são chamados de conjunto de treinamento e o programa é chamado de modelo, como em um modelo do problema de classificar spam de não spam. Como desenvolvedores, nós gostamos dessa terminologia, um modelo tem estado e precisa ser persistido, o treinamento é um processo que é executado uma vez e talvez seja reexecutado quando necessário, a classificação é a tarefa executada. Tudo faz sentido para nós.

Podemos ver que algumas das terminologias usadas nas definições acima não servem para os programadores. Tecnicamente, todos os programas que escrevemos são automações, comentando que o aprendizado de máquina aprende automaticamente não é significativo.

Handy One-liner

Então, vamos ver se podemos usar essas partes e construir uma definição de desenvolvedores de aprendizado de máquina. E se:

**Aprendizado de Máquina é o treinamento de um modelo a partir de dados que generalizam uma decisão em relação a uma medida de desempenho.**

**Treinar um modelo sugere exemplos de treinamento. Um modelo sugere estado adquirido através da experiência. Generaliza uma decisão que sugere a capacidade de tomar uma decisão baseada em insumos e antecipar insumos não vistos no futuro, para os quais será necessária uma decisão. Finalmente, contra uma medida de desempenho sugere uma necessidade direcionada e uma qualidade direcionada ao modelo sendo preparado.**

